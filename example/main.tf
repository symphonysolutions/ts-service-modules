module "dev1" {
  source           = "../"
  env_level        = "dev1"
  app_server_tag   = "8.0-alpine"
  proxy_server_tag = "1.12-alpine"
}

module "dev2" {
  source           = "../"
  env_level        = "dev2"
  app_server_tag   = "9.0-alpine"
  proxy_server_tag = "1.13-alpine"
}
